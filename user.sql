-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 11, 2018 lúc 02:22 PM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `leapic`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Facebook_ID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Google_ID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` text COLLATE utf8mb4_unicode_ci,
  `Real_Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Nation` text COLLATE utf8mb4_unicode_ci,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` text COLLATE utf8mb4_unicode_ci,
  `Gender` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address` text COLLATE utf8mb4_unicode_ci,
  `Birthday` datetime DEFAULT NULL,
  `Telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`ID`, `Facebook_ID`, `Google_ID`, `Name`, `Real_Name`, `Nation`, `Email`, `City`, `Gender`, `Address`, `Birthday`, `Telephone`, `Picture`, `Created`, `Modified`) VALUES
(31, '1948087662158842', '116488217771174416248', 'Tạ Thị Tấn', NULL, NULL, 'anhtu2002a@gmail.com', NULL, NULL, NULL, NULL, NULL, 'https://graph.facebook.com/1948087662158842/picture?type=large', '2018-10-11 02:49:01', '0000-00-00 00:00:00'),
(32, NULL, '105974493145419178397', 'Tú Phạm Anh', NULL, NULL, 'tupham2002a@gmail.com', NULL, NULL, NULL, NULL, NULL, 'https://lh6.googleusercontent.com/-MV0lxlPnc5M/AAAAAAAAAAI/AAAAAAAAAAA/AAN31DVdjO8nRtRXCtG1XloCLo9Hf17xZQ/mo/photo.jpg', '2018-10-11 02:52:27', '0000-00-00 00:00:00'),
(33, '686214145089283', '110383072343849643938', 'Tú Anh', NULL, NULL, 'tuphamanh96@gmail.com', NULL, NULL, NULL, NULL, NULL, 'https://lh3.googleusercontent.com/-PWEPRv1jZhc/AAAAAAAAAAI/AAAAAAAAAAA/AAN31DV3G3kz9z_jEUpahg8q3vQ5HOUDkw/mo/photo.jpg', '2018-10-11 04:11:54', '0000-00-00 00:00:00'),
(36, '2395993910427031', NULL, 'Phạm Anh Tú', NULL, NULL, 'anhtu2002a@yahoo.com', NULL, NULL, NULL, NULL, NULL, 'https://graph.facebook.com/2395993910427031/picture?type=large', '2018-10-11 04:23:34', '0000-00-00 00:00:00'),
(37, NULL, '114983746495633577847', 'Clone 1', NULL, NULL, 'tathitan96@gmail.com', NULL, NULL, NULL, NULL, NULL, 'https://lh6.googleusercontent.com/-9AjjOi5dUE4/AAAAAAAAAAI/AAAAAAAAAAA/AAN31DVJLmWs4BJYABnjJJYwMQPy02nqYg/mo/photo.jpg', '2018-10-11 01:49:57', '0000-00-00 00:00:00');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
