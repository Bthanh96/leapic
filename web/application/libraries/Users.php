<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users
{
	private $ID;
	private $Facebook_ID;
	private $Google_ID;
	private $Name;
	private $Real_Name;
	private $Nation;
	private $Email;
	private $City;
	private $Gender;
	private $Address;
	private $Birthday;
	private $Telephone;
	private $Picture;
	private $Create;
	private $Modified;


	/**
	 * Class Constructor
	 * @param    $ci   
	 * @param    $ID   
	 * @param    $Facebook_ID   
	 * @param    $Google_ID   
	 * @param    $Name   
	 * @param    $Nation   
	 * @param    $Email   
	 * @param    $City   
	 * @param    $Gender   
	 * @param    $Address   
	 * @param    $Birthday   
	 * @param    $Telephone   
	 * @param    $Picture   
	 * @param    $Create   
	 * @param    $Modified   
	 */
	public function __construct(ID $ID, Facebook_ID $Facebook_ID, Google_ID $Google_ID, Name $Name, Real_Name $Real_Name, Nation $Nation, Email $Email, City $City, Gender $Gender, Address $Address, Birthday $Birthday, Telephone $Telephone, Picture $Picture, Create $Create, Modified $Modified)
	{
		
		$this->ID = $ID;
		$this->Facebook_ID = $Facebook_ID;
		$this->Google_ID = $Google_ID;
		$this->Name = $Name;
		$this->Nation = $Nation;
		$this->Email = $Email;
		$this->City = $City;
		$this->Gender = $Gender;
		$this->Address = $Address;
		$this->Birthday = $Birthday;
		$this->Telephone = $Telephone;
		$this->Picture = $Picture;
		$this->Create = $Create;
		$this->Modified = $Modified;
	}


    /**
     * @return mixed
     */
    public function getID(): ID
    {
        return $this->ID;
    }

    /**
     * @return mixed
     */
    public function getFacebookID(): Facebook_ID
    {
        return $this->Facebook_ID;
    }

    /**
     * @return mixed
     */
    public function getGoogleID(): Google_ID
    {
        return $this->Google_ID;
    }

    /**
     * @return mixed
     */
    public function getName(): Name
    {
        return $this->Name;
    }

    /**
     * @return mixed
     */
    public function getRealName(): Real_Name
    {
        return $this->Real_Name;
    }

    /**
     * @return mixed
     */
    public function getNation(): Nation
    {
        return $this->Nation;
    }

    /**
     * @return mixed
     */
    public function getEmail(): Email
    {
        return $this->Email;
    }

    /**
     * @return mixed
     */
    public function getCity(): City
    {
        return $this->City;
    }

    /**
     * @return mixed
     */
    public function getGender(): Gender
    {
        return $this->Gender;
    }

    /**
     * @return mixed
     */
    public function getAddress(): Address
    {
        return $this->Address;
    }

    /**
     * @return mixed
     */
    public function getBirthday(): Birthday
    {
        return $this->Birthday;
    }

    /**
     * @return mixed
     */
    public function getTelephone(): Telephone
    {
        return $this->Telephone;
    }

    /**
     * @return mixed
     */
    public function getPicture(): Picture
    {
        return $this->Picture;
    }

    /**
     * @return mixed
     */
    public function getCreate(): Create
    {
        return $this->Create;
    }

    /**
     * @return mixed
     */
    public function getModified(): Modified
    {
        return $this->Modified;
    }
    

    /**
     * @param mixed $ID
     *
     * @return self
     */
    public function setID($ID)
    {
        $this->ID = $ID;

        return $this;
    }

    /**
     * @param mixed $Facebook_ID
     *
     * @return self
     */
    public function setFacebookID($Facebook_ID)
    {
        $this->Facebook_ID = $Facebook_ID;

        return $this;
    }

    /**
     * @param mixed $Google_ID
     *
     * @return self
     */
    public function setGoogleID($Google_ID)
    {
        $this->Google_ID = $Google_ID;

        return $this;
    }

    /**
     * @param mixed $Name
     *
     * @return self
     */
    public function setName($Name)
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @param mixed $Real_Name
     *
     * @return self
     */
    public function setRealName($Real_Name)
    {
        $this->Real_Name = $Real_Name;

        return $this;
    }

    /**
     * @param mixed $Nation
     *
     * @return self
     */
    public function setNation($Nation)
    {
        $this->Nation = $Nation;

        return $this;
    }

    /**
     * @param mixed $Email
     *
     * @return self
     */
    public function setEmail($Email)
    {
        $this->Email = $Email;

        return $this;
    }

    /**
     * @param mixed $City
     *
     * @return self
     */
    public function setCity($City)
    {
        $this->City = $City;

        return $this;
    }

    /**
     * @param mixed $Gender
     *
     * @return self
     */
    public function setGender($Gender)
    {
        $this->Gender = $Gender;

        return $this;
    }

    /**
     * @param mixed $Address
     *
     * @return self
     */
    public function setAddress($Address)
    {
        $this->Address = $Address;

        return $this;
    }

    /**
     * @param mixed $Birthday
     *
     * @return self
     */
    public function setBirthday($Birthday)
    {
        $this->Birthday = $Birthday;

        return $this;
    }

    /**
     * @param mixed $Telephone
     *
     * @return self
     */
    public function setTelephone($Telephone)
    {
        $this->Telephone = $Telephone;

        return $this;
    }

    /**
     * @param mixed $Picture
     *
     * @return self
     */
    public function setPicture($Picture)
    {
        $this->Picture = $Picture;

        return $this;
    }

    /**
     * @param mixed $Create
     *
     * @return self
     */
    public function setCreate($Create)
    {
        $this->Create = $Create;

        return $this;
    }

    /**
     * @param mixed $Modified
     *
     * @return self
     */
    public function setModified($Modified)
    {
        $this->Modified = $Modified;

        return $this;
    }
}
/* End of file Users.php */
/* Location: ./application/libraries/Users.php */
