<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	//public $variable;

	public $ID = "";
	public $Facebook_ID = "";
	public $Google_ID = "";
	public $Name = "";
	public $Real_Name = "";
	public $Nation = "";
	public $Email = "";
	public $City = "";
	public $Gender = "";
	public $Address = "";
	public $Birthday = "";
	public $Telephone = "";
	public $Picture = "";
	public $Create = "";
	public $Modified = "";



	public function __construct()
	{
		parent::__construct();
		$user = new stdclass;
		$user = (object) NULL;
	}
	public function Create_User($new_user)
	{
		extract($new_user);
		$Users = (object) $new_user;
		$cre_user = json_encode($Users);
		$this->db->insert('user', $Users);
		return true;
	}
	public function Create_User_Firebase($new_user,$database)
	{
		// extract($new_user);
		$Users = $new_user;
		// echo "<pre>";
		// //utf8_decode($Users);
		// print_r($Users);
		$ID = $Users['ID'];
		$Facebook_ID = $Users['Facebook_ID'];
		$Google_ID = $Users['Google_ID'];
		$Name = $Users['Name'];
		$Real_Name = $Users['Real_Name'];
		$Nation = $Users['Nation'];
		$Email = $Users['Email'];
		$City = $Users['City'];
		$Gender = $Users['Gender'];
		$Address = $Users['Address'];
		$Birthday = $Users['Birthday'];
		$Telephone = $Users['Telephone'];
		$Picture = $Users['Picture'];

		if ($Facebook_ID == "") {
			$UID = $Google_ID;			
		} else{
			$UID = $Facebook_ID;
		}
		echo $UID;
		$create_firebase_user = $database
		 	->getReference('Student')
		 	->update([
		 		$UID."/ID" => $ID,
		 		$UID."/Facebook_ID" => $Facebook_ID,
		 		$UID."/Google_ID" => $Google_ID,
		 		$UID."/Name"=> $Name,
		 		$UID."/Real_Name" => $Real_Name,
		 		$UID."/Nation" => $Nation,
		 		$UID."/Email"=> $Email,
		 		$UID."/City" => $City,
		 		$UID."/Gender" => $Gender,
		 		$UID."/Address" => $Address,
		 		$UID."/Birthday" => $Birthday,
		 		$UID."/Telephone" => $Telephone,
		 		$UID."/Picture" => $Picture
		 	]);
	}
	public function Update_for_FBLogin($Facebook_ID,$Email)
	{
		$this->db->set('Facebook_ID', $Facebook_ID);
		$this->db->where('Email', $Email);
		$this->db->update('user');
	}
	public function Update_for_GGLogin($Google_ID,$Email)
	{

		$this->db->set('Google_ID', $Google_ID);
		$this->db->where('Email', $Email);
		$this->db->update('user');
	}
	public function CheckDuplicateEmail($new_user,$Email)
	{
		extract($new_user);
		$Users = (object) $new_user;
		$Email = $Users->Email;
		$this->db->where('Email', $Email);
		$query = $this->db->get('user');
		$count_row = $query->num_rows();
		if($count_row>0)
		{
			return FALSE;
		} else {
			return TRUE;
		}
	}
	public function Login_User($Email)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('Email', $Email);
		$loginuser = $this->db->get();
		$loginuser = $loginuser ->row_array();
		if(count($loginuser) >0)
		{
			return $loginuser;
		} else {
			return FALSE;
		}
	}
	public function Edit_User($param,$params,$database)
	{
		$user->Real_Name = $param['user_realname'];
		$user->Nation = $param['user_country'];
		$user->City = $param['user_city'];
		$user->Gender = $param['user_gender'];
		$user->Address = $param['user_address'];
		$user->Birthday = $param['birthday_date'];
		$user->Telephone = $param['user_phone'];
		$user->Modified = $param['user_modified'];
		// $Email = $this->input->post('user_email');
		// echo $Email;
		// echo "<pre>";
		// var_dump($user);
		// $this->db->set($user);
		// $this->db->where('Email', $this->input->post('user_email'));
		// $this->db->update('user', $user);
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		echo 'UID trung khop la'.$UID;
		$update_firebase_user = $database
		 	->getReference('Student')
		 	->update([
		 		$UID."/Real_Name" => $user->Real_Name,
		 		$UID."/Nation" => $user->Nation,
		 		$UID."/City" => $user->City,
		 		$UID."/Gender" => $user->Gender,
		 		$UID."/Address" => $user->Address,
		 		$UID."/Birthday" => $user->Birthday,
		 		$UID."/Telephone" => $user->Telephone,
		 	]);
	}
	public function Detail_User($database,$params)
	{
		// $this->db->select('*');
		// $this->db->from('user');
		// $this->db->where('Email', $Email);
		// $detailuser = $this->db->get('');
		// $detailuser = $detailuser -> row_array();
		
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		$test = $database->getReference('Student/'.$UID);
		$detailuser = $test->getValue();

		// print_r($detailuser);
		return $detailuser;
	}
	public function WordLearned_ByFirebase($database,$params)
	{
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		$Wordlearned = $params['countlearned'];
		$test = $database
		 		->getReference('Student')
		 		->update([
		 			$UID."/Wordlearned" => $Wordlearned
		 		]);
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */