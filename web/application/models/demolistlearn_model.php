<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	use Kreait\Firebase;
	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;
class demolistlearn_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();

	}
	public function loadCategory()
	{
		$this->db->select('ID,Name, Icon2, Icon1');  //lay het du lieu
		$category = $this->db->get('topic');  //lay tu bang topic va luu vao bien category
		// dua ket qua thanh 1 mang du lieu
		$category= $category->result_array(); //bien doi $category thanh 1 mang
		return $category;
	}
	public function getWordsByID($ID)
	{
		$this->db->select('word.Id,word.Name,word.TopicId,word.WordType,word.Spell,word.Mean,word.Pic,word.Learned');
		$this->db->where('TopicId', $ID);
		$this->db->from('word');
		$this->db->join('topic', 'topic.ID = word.TopicId');
		$Words= $this->db->get('');  //lay tu bang words va luu vao bien words
		$Words= $Words->result(); //bien doi $words thanh 1 mang objec
		return $Words;
	}
	public function getRandomAns($ID)
	{
		$this->db->select('word.Id,word.Name,word.TopicId,word.WordType,word.Spell,word.Mean,word.Pic,word.Learned');
		$this->db->where('TopicId', $ID);
		$this->db->from('word');
		$this->db->join('topic', 'topic.ID = word.TopicId');
		$this->db->order_by('rand()');
		$this->db->limit('1');
		$RandA= $this->db->get('');  //lay tu bang words va luu vao bien words
		$RandA= $RandA->result(); //bien doi $words thanh 1 mang objec
		return $RandA;
	}
	public function create_step_for_topic($ID,$params)
	{
		$data = array(
			'userid' => $params['userID'],
			'topicid' => $ID,
			'stepnum' => 0
		);
		$this->db->where('userid', $params['userID']);
		$this->db->where('topicid', $ID);
		$q = $this->db->get('stepword');
		if ($q->num_rows() == 0) {
			$this->db->insert('stepword', $data);
		}
	}
	public function create_step_firebase($params,$database)
	{
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		$countfinish = count($params['vol']);
		if ($params['stepnum'] >= $countfinish) {
			$stepnumber = -1;
		} else {
			$stepnumber = $params['stepnum'];
		}
		// echo $countfinish;
		// echo '<br>';
		// echo $stepnumber;
		$stepID = $params['topicId'];
		$Step = $database
		 		->getReference('Step')
		 		->update([
		 			$UID."/".$stepID => $stepnumber
		 		]);
	}
	public function getstepnum($params)
	{
		$this->db->select('stepword.stepnum');
		$this->db->where('userid', $params['userID']);
		$this->db->from('stepword');
		$this->db->join('topic', 'topic.ID = stepword.topicid');
		$this->db->where('topicid', $params['topicId']);
		$stepNum = $this->db->get('');
		$stepNum = $stepNum->result_array();
		return $stepNum;
	}
	public function markLearnedWord($params)
	{
		$data = array(
			'stepnum' => $params['stepnum']
		);
		$this->db->where('userid', $params['userID']);
		$this->db->where('topicid', $params['topicId']);
		$this->db->update('stepword', $data);
		// $this->db->set('learned',$Learned);
		// $this->db->where('Id', $Id);
		// $result=$this->db->update('word');
	}
	public function check_step_firebase($database,$params)
	{
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		$stepID = $params['topicId'];
		$test = $database->getReference('Step/'.$UID.'/'.$stepID.'');
		$step = $test->getValue();
		return $step;
	}
	public function get_liststep_firebase($database,$params)
	{
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		$test = $database->getReference('Step/'.$UID.'/');
		$liststep = $test->getValue();
		// echo '<pre>';
		// print_r($liststep);
		return $liststep;
	}
	public function countLearned($database,$params)
	{
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		
	}
	public function load_list_topic()
	{
		$this->db->select('ID,Name');  //lay het du lieu
		
		$category = $this->db->get('topic');  //lay tu bang topic va luu vao bien category
		// dua ket qua thanh 1 mang du lieu
		$category= $category->result(); //bien doi $category thanh 1 mang
		return $category;
	}
	public function remove_learn_process($database,$params)
	{
		if ($params['Facebook_ID'] == "") {
			$UID = $params['Google_ID'];			
		} else{
			$UID = $params['Facebook_ID'];
		}
		// echo $countfinish;
		// echo '<br>';
		// echo $stepnumber;
		$stepID = $params['topic_id'];
		$Step = $database
		 		->getReference('Step')
		 		->update([
		 			$UID."/".$stepID => 0
		 		]);
	}
}

/* End of file demolistlearn_model.php */
/* Location: ./application/models/demolistlearn_model.php */