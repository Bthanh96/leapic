    <?php include('header.php'); ?>

    <!-- ***** Learn Area Start ***** -->
    <section class="learnword_area clearfix" id="home" >
        <div class="container h-100 ">
            <div class="row h-75 align-items-center" id="learn_group">                  
                <?php foreach ($vol1 as $row):  ?>
                    <div class="card col-sm-6 offset-sm-2 col-md-8 col-lg-5 offset-lg-3 card-block text-center word_card">
                                <span name="ID" class="word_Id" value="<?= $row->Id; ?>"></span>
                                <p name="Name" class="word_name"><?= $row->Name; ?></p>
                                <p name="Spell" class="word_spell"><?= $row->Spell; ?></p>
                                <div class="word_volume">
                                    <div class="word_volume_icon"><img src="<?php echo base_url(); ?>img/learn-img/icon-volume.png" onclick='responsiveVoice.speak("<?= $row->Name; ?>");' type='button'></img>
                                    </div>
                                </div>
                                <?php echo '<img src="data:image/png;base64,'.base64_encode($row->Pic).'" class="card-title mx-auto img-fluid word_img"  ></img>'; ?>
                                <p name="Mean" class="word_mean"><?= $row->Mean; ?></p>
                                <span  class="word_learned"><?= $row->Learned; ?></span>
                                <!-- <a href="" id="word_ex" class="card-block col-md-12 col-lg-12"><?= $row->Ex; ?></a> -->
                    </div>
                <?php endforeach ?>
                    <button class="btn btn-dark col-sm-4 offset-sm-1 col-lg-2 offset-lg-3 button" id="btn_previous" data-diff='-20' style="visibility:hidden" onclick="plusDivs(-1)">Quay lại</button>
                    <button class="btn btn-success col-sm-4 offset-sm-2 col-lg-2 offset-lg-1 button" id="btn_next" data-diff='20' onclick="plusDivs(1);">Tiếp Theo</button>   
                    <button class="btn btn-danger offset-sm-0 col-sm-4 col-lg-2 " id="btn_end" style="visibility:hidden" >ÔN TẬP</button>
            </div>
                                <?php 
                                    shuffle($vol1);
                                ?>
                    <div class="card col-sm-6 offset-sm-2 col-md-8 col-lg-5 offset-lg-3 card-block text-center exercise_card " id="baitap">
                        <span name="Ex" class="Ex_Name" value="">TRẮC NGHIỆM</span>
                        <p name="Question" class="Ex_Ques"><span id="Ex_Question"><?= $vol1[0]->Mean; ?></span> trong Tiếng Anh Là ?</p>
                        <span id="Ex_dapan"><?= $vol1[0]->Name; ?></span>
                        <ul>
                            <li><div class="Ex_Ans_Card" id="Ans_1">
                                <?php echo '<img src="data:image/png;base64,'.base64_encode($vol1[0]->Pic).'" class="card-title mx-auto img-fluid Ex_Img_card" id="Img_Ans_1" ></img>'; ?>
                                <p name="Name" class="word_name" id="word_name1"><?= $vol1[0]->Name; ?></p>
                            </div></li>
                            <li><div class="Ex_Ans_Card" id="Ans_2">                                   
                                <?php echo '<img src="data:image/png;base64,'.base64_encode($vol1[1]->Pic).'" class="card-title mx-auto img-fluid Ex_Img_card" id="Img_Ans_2" ></img>'; ?>
                                <p name="Name" class="word_name" id="word_name2"><?= $vol1[1]->Name; ?></p>
                            </div></li>
                            <li><div class="Ex_Ans_Card" id="Ans_3">
                                <?php echo '<img src="data:image/png;base64,'.base64_encode($vol1[2]->Pic).'" class="card-title mx-auto img-fluid Ex_Img_card" id="Img_Ans_3" ></img>'; ?>
                                <p name="Name" class="word_name" id="word_name3"><?= $vol1[2]->Name; ?></p>
                            </div></li>
                            <li><div class="Ex_Ans_Card" id="Ans_4">
                                <?php echo '<img src="data:image/png;base64,'.base64_encode($vol1[3]->Pic).'" class="card-title mx-auto img-fluid Ex_Img_card" id="Img_Ans_4" ></img>'; ?>
                                <p name="Name" class="word_name" id="word_name4"><?= $vol1[3]->Name; ?></p>
                            </div></li>
                        </ul>
                    </div>
                    <div class="card col-sm-6 offset-sm-2 col-md-8 col-lg-5 offset-lg-3 card-block text-center exercise_card2 " id="baitap2">
                        <span name="Ex2" class="Ex_Name" value="">NỐI TỪ</span>
                        <p name="Question2" class="Ex_Ques">Nối các cặp từ có cùng nghĩa</p>
                        <table id="baitap_card">
                            
                            <tr>
                                <td class="column1" id="col1_row1">
                                    <div class="Ex2_Ans_Name_Card" id="Ex2_Name1" onclick="chonname(1)"><?= $vol1[1]->Name ?></div>
                                    <div class="Ex2_Dapan" id="Ex2_Dapan1"><?= $vol1[1]->Mean ?></div>
                                </td>
                                <td class="column2">
                                    <div class="Ex2_Ans_Mean_Card" id="Ex2_Mean1" onclick="chonnghia(1)"><?= $vol1[3]->Mean ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <div class="Ex2_Ans_Name_Card" id="Ex2_Name2" onclick="chonname(2)"><?= $vol1[0]->Name ?></div>
                                    <div class="Ex2_Dapan" id="Ex2_Dapan2"><?= $vol1[0]->Mean ?></div>
                                </td>
                                <td class="column2">
                                    <div class="Ex2_Ans_Mean_Card" id="Ex2_Mean2" onclick="chonnghia(2)"><?= $vol1[1]->Mean ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <div class="Ex2_Ans_Name_Card" id="Ex2_Name3" onclick="chonname(3)"><?= $vol1[3]->Name ?></div>
                                    <div class="Ex2_Dapan" id="Ex2_Dapan3"><?= $vol1[3]->Mean ?></div>
                                </td>
                                <td class="column2">
                                    <div class="Ex2_Ans_Mean_Card" id="Ex2_Mean3" onclick="chonnghia(3)"><?= $vol1[2]->Mean ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <div class="Ex2_Ans_Name_Card" id="Ex2_Name4" onclick="chonname(4)"><?= $vol1[2]->Name ?></div>
                                    <div class="Ex2_Dapan" id="Ex2_Dapan4"><?= $vol1[2]->Mean ?></div>
                                </td>
                                <td class="column2">
                                    <div class="Ex2_Ans_Mean_Card" id="Ex2_Mean4" onclick="chonnghia(4)"><?= $vol1[0]->Mean ?></div>
                                </td>
                            </tr>
                        </table>
                        <button class="btn btn-danger cd-popup-trigger" id="btn_ketthuc" style="visibility: hidden;">KẾT THÚC</button>
                    </div>

        </div>
        <progress id="progress" value='20' max='100' ></progress>
        <div class="cd-popup" role="alert">
          <div class="cd-popup-container">
            <img id="cd-popup-img" src="<?php echo base_url(); ?>img/learn-img/icon-congra.png"></img>
            <h1 id="cd-popup-congra">Chúc mừng</h1>
            <p id="cd-popup-count">Bạn đã học được <span id="showCount"></span> từ vựng</p>

            <br>
              <ul class="cd-buttons">
                <li><a href="<?= base_url(); ?>index.php/demolistlearn/markasLearned">KẾT THÚC</a></li>
                <li><a href="<?= base_url(); ?>index.php/demolistlearn/markasLearned_and_continue">HỌC TIẾP</a></li>
              </ul>
            <a href="#0" class="cd-popup-close img-replace"></a>
          </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
    </section>
    <!-- ***** Learn Area End ***** -->
    <?php include('footer.php'); ?>
