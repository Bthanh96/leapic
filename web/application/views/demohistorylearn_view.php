<?php include('header.php'); ?>
<section class="userpage_area clearfix" id="home">
		<div class="container h-100 ">
			<div class="row h-75 align-items-center">
				<div class="card col-sm-6 offset-sm-2 col-md-6 col-lg-2 offset-lg-2 card-block userdetail_card">
					<div class="history_head">
                        <h3 class="history_title">DANH SÁCH TOPIC ĐÃ HỌC</h3>
                    </div>
                    <!-- List Learned Begin -->
                    <div class="history_list">
                        <div class="row">
                            <h5 class="topic_name_title col-sm">Tên Bài Học</h5>
                            <h5 class="topic_learned_title col-sm">Tình Trạng</h5>
                            <h5 class="topic_setting_title col-sm">Tùy chỉnh<h5>
                        </div>
                        <div class="topic_table">
                            <?php foreach ($data as $key => $value): ?>
                                <div class="row div_topic">
                                    <span class="topic_name col-sm"><?= $value->Name ?></span>
                                    <span class="topic_learned col-sm"><?= $value->learn_process ?></span>
                                    <span class="topic_setting col-sm"><button class="topic_setting_btn btn btn-info" onclick="window.location='<?php echo base_url(); ?>index.php/demouser/remove_learn_process/<?= $value->ID ?>'" <?php if ($value->learn_process == '0/24'){ ?> disabled <?php }?>>Học Lại</button></span>
                                    <hr>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!-- List Learned End -->
				</div>
			</div>
		</div>
	</section>
<?php include('footer.php'); ?>