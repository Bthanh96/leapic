<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>LeaPic - Learn English by Picture</title>

    <!-- Favicon -->
    <link rel="icon" href="/WebAppLeng/img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="<?php echo base_url(); ?>style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
    
    <link href="<?php echo base_url(); ?>dist/css/swiper.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Datepicker -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css"><script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
    $( function() {
      $( "#birthday_date" ).datepicker({
        format: 'yyyy/mm/dd',
        startDate: '1900/01/01',
        autoclose: true,
        todayBtn: true,
        todayHighlight: true
      });
    } );
    </script>

    <script>
        $('#user_gender').val('<?=$r_item['item_status'];?>');
    </script>

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>
    <!-- ***** Header Area Start ***** -->
    <header class="header_area animated">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-10 col-lg-10">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="<?= base_url(); ?>index.php/homepage">LeaP.</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse col-lg-10" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item"><a class="nav-link" href="<?= base_url(); ?>index.php/homepage">Home</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<?= base_url(); ?>index.php/demolistlearn">Learn</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<?= base_url(); ?>index.php/demogame">Game</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#screenshot">Screenshot</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#team">Team</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                                </ul>
                                <!-- Reponsive Buton -->
                                <div class="sing-up-button d-lg-none d-md-none">
                                    <a href="<?= base_url(); ?>index.php/demouser/loginpage">Log in</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signin btn -->
                <?php if(isset($_SESSION['user'])): ?>
                    <div class="col-12 col-lg-2">
                        <div class="sing-up-button d-none d-lg-block">
                            <a href="<?= base_url(); ?>index.php/demouser/user_detail">My User</a>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-12 col-lg-2">
                        <div class="sing-up-button d-none d-lg-block">
                            <a href="<?= base_url(); ?>index.php/demouser/loginpage">Log In</a>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->