<?php include('header.php'); ?>
	
	<!-- ***** Listlearn Area Start ***** -->
	<section class="userpage_area clearfix" id="home">
		<div class="container h-100 ">
			<div class="row h-100 align-items-center" id="userdetail_card">
				<div class="card col-sm-6 offset-sm-2 col-md-8 col-lg-5 offset-lg-2 card-block userdetail_card">
					<div class="user_head">
						<img src='<?= $userdetail['Picture'] ?>' class='user_avatar rounded-circle'><span class="user_name"><?= $userdetail['Name'] ?></span>					
						<a href="<?= base_url(); ?>index.php/demouser/get_history_learned" class="badge badge-light lesson_count"> Số bài đã học <br> <?= $userdetail['Wordlearned'] ?></a>
					</div>

					<!-- Begin User Info Form -->
					<div class="user_info">
						<form class="form-row userinfo_form" action="<?php echo base_url(); ?>index.php/demouser/user_edit" method="post" name="user_form">
							<div class="form-group col-lg-6">
								<label for="user_name">Họ và Tên</label>
								<input type="text" class="form-control" name="user_realname" value="<?php if (isset($userdetail['Real_Name'])) echo $userdetail['Real_Name']; ?>" placeholder="Nhập Tên">
							</div>
							<div class="form-group col-lg-6">
								<label for="user_country">Quốc gia</label>
								<input type="text" class="form-control" name="user_country" value="<?php if (isset($userdetail['Nation'])) echo $userdetail['Nation']; ?>" placeholder="Nhập quốc gia">
							</div>
							<div class="form-group col-lg-6">
								<label for="user_email">Email</label>
								<input type="text" class="form-control " id='user_email' name="user_email" value="<?php if (isset($userdetail['Email'])) echo $userdetail['Email']; ?>" readonly>
							</div>
							<div class="form-group col-lg-6">
								<label for="user_city">Thành Phố</label>
								<input type="text" class="form-control" name="user_city" value="<?php if (isset($userdetail['City'])) echo $userdetail['City']; ?>" placeholder="Nhập Thành Phố">
							</div>
							<div class="form-group col-lg-6">
								<label for="user_gender">Giới tính</label>
								<select class="form-control" name="user_gender"  value="<?php if (isset($userdetail['Gender'])) echo $userdetail['Gender']; ?>">
									<option value="undefine">Không Xác Định</option>
									<option value="male">Nam</option>
									<option value="female">Nữ</option>
								</select>
							</div>
							<div class="form-group col-lg-6">
								<label for="user_address">Địa chỉ</label>
								<input type="text" class="form-control" name="user_address" value="<?php if (isset($userdetail['Address'])) echo $userdetail['Address']; ?>" placeholder="Nhập Địa Chỉ">
							</div>
							<div class="form-group col-lg-6">
								<label for="birthday_date">Ngày sinh</label>
								<input type="text" class="form-control" id='birthday_date' name="birthday_date" value="<?php if (isset($userdetail['Birthday'])) echo $userdetail['Birthday']; ?>">
									
							</div>
							<div class="form-group col-lg-6 submit">
								<label for="user_phone">Điện thoại</label>
								<input type="tel" class="form-control" name="user_phone" value="<?php if (isset($userdetail['Telephone'])) echo $userdetail['Telephone']; ?>" placeholder="Nhập SĐT">
							</div>
							<div class="form-group">
							<button class="btn btn-success save_user_info" type="submit">Lưu Thông Tin</button>
							</div>
							<div class="form-group">
							<button class="btn btn-info logout_user" type="button"><a href="Logout" class="logout text-white">Logout</a></button>
							</div>
						</form>
					</div>
					<!-- End User Info Form -->
				</div>
			</div>
		</div>
	</section>
	<!-- ***** Listlearn Area End ***** -->
<?php include('footer.php'); ?>