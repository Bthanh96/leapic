<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	use Kreait\Firebase;
	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;
class Demouser extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		require_once APPPATH.'third_party/src/Google_Client.php';
		require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';
		$this->load->model('User_model');
		$this->load->model('demolistlearn_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function index()
	{
		
	}

	public function loginpage()
	{
		$this->load->view('loginpage_view');
	}
	
	public function fblogin(){
		if (!session_id()) {
		    session_start();
		}
	    $fb = new Facebook\Facebook([
	          'app_id' => '2312722738949095',
	          'app_secret' => 'f55e863cb8ecb5be5849c738d60c452a',
	          'default_graph_version' => 'v2.5',
	        ]);

	   $helper = $fb->getRedirectLoginHelper();

	   $permissions = ['email']; 
	// For more permissions like user location etc you need to send your application for review

	   $loginUrl = $helper->getLoginUrl('https://localhost/WebAppLeng/index.php/Demouser/fbcallback', $permissions);

	   header("location: ".$loginUrl);
	   echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
	}
	
	public function fbcallback(){
		if(!session_id()) {
		    session_start();
		}

        $fb = new Facebook\Facebook([
        'app_id' => '2312722738949095',
        'app_secret' => 'f55e863cb8ecb5be5849c738d60c452a',
        'default_graph_version' => 'v2.5',
        ]);
        
        $helper = $fb->getRedirectLoginHelper();  
  		
        try {  
            
            $accessToken = $helper->getAccessToken();  
            
        }catch(Facebook\Exceptions\FacebookResponseException $e) {  
          // When Graph returns an error  
          echo 'Graph returned an error: ' . $e->getMessage();  
          exit;  
        } catch(Facebook\Exceptions\FacebookSDKException $e) {  
          // When validation fails or other local issues  
          echo 'Facebook SDK returned an error: ' . $e->getMessage();  
          exit;  
        }  
 		
        if (! isset($accessToken)) {
 			if ($helper->getError()) {
			    header('HTTP/1.0 401 Unauthorized');
			    echo "Error: " . $helper->getError() . "\n";
			    echo "Error Code: " . $helper->getErrorCode() . "\n";
			    echo "Error Reason: " . $helper->getErrorReason() . "\n";
			    echo "Error Description: " . $helper->getErrorDescription() . "\n";
			} else {
		    header('HTTP/1.0 400 Bad Request');
		    echo 'Bad request';
		  	}
		  	exit;
		}

		// Logged in

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId('2312722738949095'); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived()) {
		  // Exchanges a short-lived access token for a long-lived one
		  try {
		    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (Facebook\Exceptions\FacebookSDKException $e) {
		    echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
		    exit;
		  }

		  echo '<h3>Long-lived</h3>';
		  var_dump($accessToken->getValue());
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;
 
        try {
          // Get the Facebook\GraphNodes\GraphUser object for the current user.
          // If you provided a 'default_access_token', the '{access-token}' is optional.
          $response = $fb->get('/me?fields=id,name,email,first_name,last_name,birthday,location', $accessToken);
         // print_r($response);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'ERROR: Graph ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'ERROR: validation fails ' . $e->getMessage();
          exit;
        }
    
        // User Information Retrival begins................................................
        $me = $response->getGraphUser();

        
        // $location = $me->getProperty('location');
        // echo "Full Name: ".$me->getProperty('name')."<br>";
        // echo "First Name: ".$me->getProperty('first_name')."<br>";
        // echo "Last Name: ".$me->getProperty('last_name')."<br>";
        // echo "Email: ".$me->getProperty('email')."<br>";
        // echo "Facebook ID: <a href='https://www.facebook.com/".$me->getProperty('id')."' target='_blank'>".$me->getProperty('id')."</a>"."<br>";
        // $user_facebookid = $me->getProperty('id');
        // echo "</br><img src='//graph.facebook.com/$user_facebookid/picture?type=large'> ";
        // echo "</br></br>Access Token : </br>".$accessToken;    
        $Email = $me->getProperty('email');
        $Facebook_ID = $me->getProperty('id');
        $Img = "https://graph.facebook.com/".$Facebook_ID."/picture?type=large";
        $new_user = array(
        	'Name' => $me->getProperty('name'),
        	'Email' => $me->getProperty('email'),
        	'Facebook_ID' =>$me->getProperty('id'),
        	'Real_Name' => '',
        	'Nation' => '',
        	'City' => '',
        	'Gender' => '',
        	'Address' => '',
        	'Birthday' => '',
        	'Telephone' => '',
        	'Modified' => '',
        	'Picture' => $Img,
        	'Created' => date("Y-m-d h:i:sa")
        );
        // $this->load->view('demouser_view',$new_user);
        echo "<pre>";
        // var_dump($new_user);
        if($this->User_model->CheckDuplicateEmail($new_user,$Email))
        {
        	if($this->User_model->Create_User($new_user)) // call the method from the model
			{
			    echo 'thanh cong';
			    $user_result = $this->User_model->Login_User($Email);
			    if($user_result)
				{
				    $this->session->set_userdata('user',$user_result);    
				    var_dump($user_result);
				    $ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
					    ->withServiceAccount($ServiceAccount)
					    // The following line is optional if the project id in your credentials file
					    // is identical to the subdomain of your Firebase project. If you need it,
					    // make sure to replace the URL with the URL of your project.
					    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					    ->create();
					$database = $firebase->getDatabase();
					$new_user = $this->session->userdata('user');	
					$this->User_model->Create_User_Firebase($new_user,$database);
				    redirect("Demouser/user_detail");
				}
			}
			else
			{
			    echo 'loi';
			}
        } else {
        	$this->User_model->Update_for_FBLogin($Facebook_ID,$Email);
        	$user_result = $this->User_model->Login_User($Email);
			if($user_result)
			{
				$this->session->set_userdata('user',$user_result);
				var_dump($user_result) ;
				$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
				$firebase = (new Factory)
					->withServiceAccount($ServiceAccount)
					// The following line is optional if the project id in your credentials file
					// is identical to the subdomain of your Firebase project. If you need it,
					// make sure to replace the URL with the URL of your project.
					->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					->create();
				$database = $firebase->getDatabase();
				$new_user = $this->session->userdata('user');	
				$this->User_model->Create_User_Firebase($new_user,$database);
				redirect("Demouser/user_detail");
			}
        }
    }
    public function google_login()
	{
		$clientId = '877156801563-fu6ip1kve6o66021jl213uhu4q24jecu.apps.googleusercontent.com'; //Google client ID
		$clientSecret = 'IjdDpnX3y_qFoVBA295gVuVq'; //Google client secret
		$redirectURL = 'https://localhost/WebAppLeng/index.php/Demouser/google_login/';

		//Call Google API
		$gClient = new Google_Client();
		$gClient->setApplicationName('LeaPic');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		$gClient->setRedirectUri($redirectURL);
		$google_oauthV2 = new Google_Oauth2Service($gClient);

		if(isset($_GET['code']))
		{
			$gClient->authenticate($_GET['code']);
			$_SESSION['token'] = $gClient->getAccessToken();
			header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
		}

		if (isset($_SESSION['token'])) 
		{
			$gClient->setAccessToken($_SESSION['token']);
		}

		if ($gClient->getAccessToken()) {
			$userProfile = $google_oauthV2->userinfo->get();
			$Email = $userProfile['email'];
			$Google_ID = $userProfile['id'];
			$new_user = array(
				'Name' => $userProfile['name'],
				'Email' => $userProfile['email'],
				'Google_ID' => $userProfile['id'],
				'Real_Name' => '',
	        	'Nation' => '',
	        	'City' => '',
	        	'Gender' => '',
	        	'Address' => '',
	        	'Birthday' => '',
	        	'Telephone' => '',
	        	'Modified' => '',
				'Picture' => $userProfile['picture'],
				'Created' => date("Y-m-d h:i:sa")
			);
			echo "<pre>";
			var_dump($userProfile);
			echo "Hàng 2";
			var_dump($new_user);
			echo '</pre>';
			if($this->User_model->CheckDuplicateEmail($new_user,$Email)){
				if($this->User_model->Create_User($new_user)) // call the method from the model
			    {
			        echo 'thanh cong';
			        $user_result = $this->User_model->Login_User($Email);
			        if($user_result)
				    {
				        $this->session->set_userdata('user',$user_result);
				        var_dump($user_result);
				        $ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
						$firebase = (new Factory)
						    ->withServiceAccount($ServiceAccount)
						    // The following line is optional if the project id in your credentials file
						    // is identical to the subdomain of your Firebase project. If you need it,
						    // make sure to replace the URL with the URL of your project.
						    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
						    ->create();
						$database = $firebase->getDatabase();
						$new_user = $this->session->userdata('user');	
						$this->User_model->Create_User_Firebase($new_user,$database);
				        redirect("Demouser/user_detail");
				    }
			    }
			    else
			    {
			        echo 'loi';
			    }		
				die;
			} else {
	        	$this->User_model->Update_for_GGLogin($Google_ID,$Email);
	        	$user_result = $this->User_model->Login_User($Email);
			    if($user_result)
			    {
			        $this->session->set_userdata('user',$user_result);  
			        var_dump($user_result);
			        $ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
						->withServiceAccount($ServiceAccount)
						// The following line is optional if the project id in your credentials file
						// is identical to the subdomain of your Firebase project. If you need it,
						// make sure to replace the URL with the URL of your project.
						->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
						->create();
					$database = $firebase->getDatabase();
					$new_user = $this->session->userdata('user');	
					$this->User_model->Create_User_Firebase($new_user,$database);
			        redirect("Demouser/user_detail");
			    }
	        }
		} 
		else 
		{
			$url = $gClient->createAuthUrl();
			header("Location: $url");
			exit;

		}
	}
	public function user_detail()
	{
		if (!$this->session->userdata['user']) {
			redirect('Demouser/loginpage');
		}else{
			$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
			$firebase = (new Factory)
					->withServiceAccount($ServiceAccount)
					// The following line is optional if the project id in your credentials file
					// is identical to the subdomain of your Firebase project. If you need it,
					// make sure to replace the URL with the URL of your project.
					->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					->create();
					$database = $firebase->getDatabase();
			$this->WordLearned();
			$logeduser = $this->session->userdata['user'];
			$params['Facebook_ID'] = $logeduser['Facebook_ID'];
			$params['Google_ID'] = $logeduser['Google_ID'];
			$Email = $logeduser["Email"];
			$userdetail['userdetail'] = $this->User_model->Detail_User($database,$params);	
			// echo '<pre>';
			// print_r($userdetail);
			$this->load->view('Demouser_view',$userdetail);
		}
		
	}
	public function logout()
	{
		$this->session->unset_userdata('user');
		redirect('Homepage');
	}
	public function user_edit()
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
		$firebase = (new Factory)
				->withServiceAccount($ServiceAccount)
				// The following line is optional if the project id in your credentials file
				// is identical to the subdomain of your Firebase project. If you need it,
				// make sure to replace the URL with the URL of your project.
				->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
				->create();
				$database = $firebase->getDatabase();
		$logeduser = $this->session->userdata['user'];
		$params['Facebook_ID'] = $logeduser['Facebook_ID'];
		$params['Google_ID'] = $logeduser['Google_ID'];
		$this->form_validation->set_rules('user_phone','user_phone','numeric');
		if($this->form_validation->run() == TRUE){
		$param['user_realname'] = $this->input->post('user_realname');
		$param['user_country'] = $this->input->post('user_country');
		$param['user_city'] = $this->input->post('user_city');
		$param['user_gender'] = $this->input->post('user_gender');
		$param['user_address'] = $this->input->post('user_address');
		$param['birthday_date'] = $this->input->post('birthday_date');
		$param['user_phone'] = $this->input->post('user_phone');
		$param['user_modified'] = date("Y-m-d h:i:sa");
		}else
		{
			echo "Loi";
		}
		$this->User_model->Edit_User($param,$params,$database);
		redirect('Demouser/user_detail');
	}
	public function WordLearned()
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
					    ->withServiceAccount($ServiceAccount)
					    // The following line is optional if the project id in your credentials file
					    // is identical to the subdomain of your Firebase project. If you need it,
					    // make sure to replace the URL with the URL of your project.
					    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					    ->create();
					$database = $firebase->getDatabase();
		$logeduser = $this->session->userdata['user'];
		$params['Facebook_ID'] = $logeduser['Facebook_ID'];
		$params['Google_ID'] = $logeduser['Google_ID'];
		$params['userID'] = $logeduser['ID'];
		$liststep['liststep'] = $this-> demolistlearn_model -> get_liststep_firebase($database,$params);
		$_SESSION['liststep'] = $liststep;
		$countwordlearned = 0;
		// echo $liststep['liststep'][3];
		if (isset($liststep['liststep'])) {
			foreach ($liststep['liststep'] as $list) {
			if ($list == -1) {
				$countwordlearned ++;
			}
		}
		}
		$params['countlearned'] = $countwordlearned;
		$this-> User_model ->WordLearned_ByFirebase($database,$params);
	}
	public function get_history_learned()
	{
		if (!$this->session->userdata['user']) {
			redirect('Demouser/loginpage');
		}else{
			$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
						$firebase = (new Factory)
							->withServiceAccount($ServiceAccount)
							// The following line is optional if the project id in your credentials file
							// is identical to the subdomain of your Firebase project. If you need it,
							// make sure to replace the URL with the URL of your project.
							->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
							->create();
						$database = $firebase->getDatabase();
			$logeduser = $this->session->userdata['user'];
			$params['Facebook_ID'] = $logeduser['Facebook_ID'];
			$params['Google_ID'] = $logeduser['Google_ID'];
			$params['userID'] = $logeduser['ID'];
			$liststep['liststep'] = $this-> demolistlearn_model -> get_liststep_firebase($database,$params);
			$data['data'] = $this-> demolistlearn_model ->load_list_topic();
			$count_list_topic = count($data['data']);
			foreach ($data['data'] as $list_topic) {
				$list_topic->learn_process = '0';
				foreach ($liststep['liststep'] as $key => $value) {
					if ($list_topic->ID == $key)
					{
						$list_topic->learn_process = $value;
					}
				}
				if ($list_topic->learn_process == '-1')
				{
					$list_topic->learn_process = "{$count_list_topic}/{$count_list_topic}";
				}else{
					$list_topic->learn_process = "{$list_topic->learn_process}/{$count_list_topic}";
				}
			}
			// echo "<pre>";
			// print_r ($data['data']);
			// echo "</pre>";
			$data['data'] = (array) $data['data'];
			$this->load->view('demohistorylearn_view',$data);
		}
	}
	public function remove_learn_process($topic_id)
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
						$firebase = (new Factory)
							->withServiceAccount($ServiceAccount)
							// The following line is optional if the project id in your credentials file
							// is identical to the subdomain of your Firebase project. If you need it,
							// make sure to replace the URL with the URL of your project.
							->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
							->create();
						$database = $firebase->getDatabase();
		$params['topic_id'] = $topic_id;
		$logeduser = $this->session->userdata['user'];
		$params['Facebook_ID'] = $logeduser['Facebook_ID'];
		$params['Google_ID'] = $logeduser['Google_ID'];
		$this-> demolistlearn_model ->remove_learn_process($database,$params);
		redirect("Demouser/get_history_learned");
	}
}

/* End of file Demouser.php */
/* Location: ./application/controllers/Demouser.php */