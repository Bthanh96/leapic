<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	use Kreait\Firebase;
	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;
class Demolistlearn extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('demolistlearn_model');
	}

	public function index()
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
					    ->withServiceAccount($ServiceAccount)
					    // The following line is optional if the project id in your credentials file
					    // is identical to the subdomain of your Firebase project. If you need it,
					    // make sure to replace the URL with the URL of your project.
					    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					    ->create();
					$database = $firebase->getDatabase();
		//$this->load->view('demolistlearn_view');
		//load model
		//goi ham loadCategory trong model
		if (!$this->session->userdata['user']) {
			redirect('Demouser/loginpage');
		}else{
			$logeduser = $this->session->userdata['user'];
			$params['Facebook_ID'] = $logeduser['Facebook_ID'];
			$params['Google_ID'] = $logeduser['Google_ID'];
			$danhsach['danhsach'] = $this->demolistlearn_model->loadCategory();
			$liststep['liststep'] = $this-> demolistlearn_model -> get_liststep_firebase($database,$params);
			$_SESSION['liststep'] = $liststep;
			$this->load->view('demolistlearn_view', $danhsach);
		}
	}
	public function detailWords($idword)
	{
		$vol['vol'] = $this -> demolistlearn_model -> getWordsByID($idword);
		$_SESSION['count'] = count($vol['vol']);
		$_SESSION['TopicId'] = $vol['vol'][0]->TopicId;
		if ($this->session->userdata['user']) {
			$logeduser = $this->session->userdata['user'];
			$params['userID'] = $logeduser['ID'];
			$params['topicId'] = $_SESSION['TopicId'];
			// $this->demolistlearn_model->create_step_for_topic($idword,$params);
			$gettedstepnum = $this->checklearned_by_firebase();
			if ($gettedstepnum == -1) {
				?>
				<script type="text/javascript">
				    alert("Bài Đã Học Xong");
				    window.location.href = "<?php echo site_url('demolistlearn'); ?>";
				</script>
			<?php
			} elseif ($gettedstepnum == 30) {
				$vol1['vol1'] = array_slice($vol['vol'], 30, 5);
			} elseif ($gettedstepnum == 25) {
				$vol1['vol1'] = array_slice($vol['vol'], 25, 5);
			} elseif ($gettedstepnum == 20) {
				$vol1['vol1'] = array_slice($vol['vol'], 20, 5);
			} elseif ($gettedstepnum == 15) {
				$vol1['vol1'] = array_slice($vol['vol'], 15, 5);
			} elseif ($gettedstepnum == 10) {
				$vol1['vol1'] = array_slice($vol['vol'], 10, 5);
			} elseif ($gettedstepnum == 5) {
				$vol1['vol1'] = array_slice($vol['vol'], 5, 5);
			} elseif ($gettedstepnum == '') {
				$vol1['vol1'] = array_slice($vol['vol'], 0, 5);
			}
		} else{
			redirect('Demouser/loginpage');
		}
		$_SESSION['vol'] = $vol['vol'];
		$_SESSION['vol1'] = $vol1['vol1'];
		$this->load->view('demolearn_view', $vol1, FALSE);
	}
	public function markasLearned()
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
					    ->withServiceAccount($ServiceAccount)
					    // The following line is optional if the project id in your credentials file
					    // is identical to the subdomain of your Firebase project. If you need it,
					    // make sure to replace the URL with the URL of your project.
					    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					    ->create();
					$database = $firebase->getDatabase();
		$params['topicId'] = $_SESSION['TopicId'] ;
		$params['vol'] = $_SESSION['vol'];
		$params['vol1'] = $_SESSION['vol1'];
		$logeduser = $this->session->userdata['user'];
		$params['Facebook_ID'] = $logeduser['Facebook_ID'];
		$params['Google_ID'] = $logeduser['Google_ID'];
		$params['userID'] = $logeduser['ID'];
		$count = count($params['vol1']);
		$step = $this-> demolistlearn_model -> check_step_firebase($database,$params);
		$step = $step + $count;
		$params['stepnum'] = $step;
		// $this -> demolistlearn_model -> markLearnedWord($params);
		$this -> demolistlearn_model -> create_step_firebase($params,$database);

		redirect('/demolistlearn');
	}
	public function checklearned()
	{
		$logeduser = $this->session->userdata['user'];
		$params['userID'] = $logeduser['ID'];
		$params['topicId'] = $_SESSION['TopicId'] ;
		$temp['temp'] = $this-> demolistlearn_model -> getstepnum($params);
		$gettedstep = $temp['temp'][0]['stepnum'];
		// echo '<pre>';
		// echo $temp['temp'][0]['stepnum'];
		return $gettedstep;
	}
	public function checklearned_by_firebase()
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
					    ->withServiceAccount($ServiceAccount)
					    // The following line is optional if the project id in your credentials file
					    // is identical to the subdomain of your Firebase project. If you need it,
					    // make sure to replace the URL with the URL of your project.
					    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					    ->create();
					$database = $firebase->getDatabase();
		$logeduser = $this->session->userdata['user'];
		$params['Facebook_ID'] = $logeduser['Facebook_ID'];
		$params['Google_ID'] = $logeduser['Google_ID'];
		$params['topicId'] = $_SESSION['TopicId'] ;
		$stepnumber = $this-> demolistlearn_model -> check_step_firebase($database,$params);
		return $stepnumber;
	}
	public function markasLearned_and_continue()
	{
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');
					$firebase = (new Factory)
					    ->withServiceAccount($ServiceAccount)
					    // The following line is optional if the project id in your credentials file
					    // is identical to the subdomain of your Firebase project. If you need it,
					    // make sure to replace the URL with the URL of your project.
					    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
					    ->create();
					$database = $firebase->getDatabase();
		$params['topicId'] = $_SESSION['TopicId'] ;
		$params['vol'] = $_SESSION['vol'];
		$params['vol1'] = $_SESSION['vol1'];
		$logeduser = $this->session->userdata['user'];
		$params['Facebook_ID'] = $logeduser['Facebook_ID'];
		$params['Google_ID'] = $logeduser['Google_ID'];
		$params['userID'] = $logeduser['ID'];
		$count = count($params['vol1']);
		$step = $this-> demolistlearn_model -> check_step_firebase($database,$params);
		$step = $step + $count;
		$params['stepnum'] = $step;
		$this -> demolistlearn_model -> create_step_firebase($params,$database);
		redirect('demolistlearn/detailWords/'.$params['topicId'],'refresh');
	}

}

/* End of file demolistlearn.php */
/* Location: ./application/controllers/demolistlearn.php */