<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;
class FirebaseUser extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		// require FCPATH.'Application\vendor\autoload.php';
		// require_once APPPATH.'kreait\Firebase\Factory';
		// require_once APPPATH.'kreait\Firebase\ServiceAccount';
		
	}

	public function index()
	{
		
	}
	public function Firebase_user()
	{
		
		$ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'\firebase\leapic-65b48-d2d09b0dad81.json');

		$firebase = (new Factory)
		    ->withServiceAccount($ServiceAccount)
		    // The following line is optional if the project id in your credentials file
		    // is identical to the subdomain of your Firebase project. If you need it,
		    // make sure to replace the URL with the URL of your project.
		    ->withDatabaseUri('https://leapic-65b48.firebaseio.com/')
		    ->create();

		$database = $firebase->getDatabase();		
	}

}

/* End of file Firebase.php */
/* Location: ./application/controllers/Firebase.php */