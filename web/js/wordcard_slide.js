    var showCount = $("#showCount");
    var slideIndex = 1;
    var slideCount = 1;
    var slideTotal = document.getElementsByClassName("word_card").length;
    console.log(slideTotal);

    $(document).ready(function() {
      $('.exercise_card').hide();
      $('.exercise_card2').hide();
      
    });

    showDivs(slideIndex);
    console.log(slideCount);
    function plusDivs(n) {
      showDivs(slideIndex += n);
      slideCount = slideCount < slideTotal ? slideCount + Number(n) : slideTotal;
      if(slideCount < 1){
        slideCount = 1;
      }else{
        document.getElementById("btn_previous").style.visibility = 'visible';
      }
      if(slideCount == 5){
        document.getElementById("btn_next").style.visibility = 'hidden';
        document.getElementById("btn_previous").style.visibility = 'hidden';
        document.getElementById("btn_end").style.visibility = 'visible';
      }else{
        document.getElementById("btn_end").style.visibility = 'hidden';
        document.getElementById("btn_next").style.visibility = 'visible';
      }
      if(slideCount == 1){
        document.getElementById("btn_previous").style.visibility = 'hidden';
      }
      console.log(slideCount);
    }

    function showDivs(n) {
      var i;
      var x = document.getElementsByClassName("word_card");
      if (n > x.length) {
        slideIndex = 1
      }    
      if (n < 1) {
        slideIndex = x.length
      }
      for (i = 0; i < x.length; i++) {
         x[i].style.display = "none";   

      }
      x[slideIndex-1].style.display = "block";

    }

    jQuery(document).ready(function($){
      //open popup
      $('.cd-popup-trigger').on('click', function(event){
        event.preventDefault();
        $('.cd-popup').addClass('is-visible');
        showCount.text(slideCount);
        
      });
      
      //close popup
      $('.cd-popup').on('click', function(event){
        if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
          event.preventDefault();
          $(this).removeClass('is-visible');
        }
      });
      $('#cd-popup-cancel').on('click', function(event){
        if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
          event.preventDefault();
          $(this).removeClass('is-visible');
        }
      });
      //close popup when clicking the esc keyboard button
      $(document).keyup(function(event){
          if(event.which=='27'){
            $('.cd-popup').removeClass('is-visible');
          }
        });
    });


    // changing progressbar when button is clicked
    $(".button").click(function () {
        animateProgress(parseInt($(this).data('diff')));
    });

    // animate progress by a step indicated by diff
    function animateProgress(diff) {
        var currValue = $("#progress").val();
        var toValue = currValue + diff;
        
        toValue = toValue < 0 ? 0 : toValue;
        toValue = toValue > 100 ? 100 : toValue;

        $("#progress").animate({'value': toValue}, 100);
    }
    $("#btn_end").on('click',function(){
      $('.word_card').hide();
      $('.exercise_card').show();
      document.getElementById("btn_end").style.visibility = 'hidden';

    })

    