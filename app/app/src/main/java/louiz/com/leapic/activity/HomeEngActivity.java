package louiz.com.leapic.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import louiz.com.leapic.R;
import louiz.com.leapic.adapter.HomeFragmentAdaper;
import louiz.com.leapic.model.Student;

public class HomeEngActivity extends AppCompatActivity {


    TabLayout tabLayout;
    ViewPager viewPager;
    HomeFragmentAdaper adapter;
    String name,id,email;
    public static final String MY_PREFS_NAME = "userData";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_eng);
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        email = intent.getStringExtra("email");
        id = intent.getStringExtra("id");
        if (id == null) {
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            name = prefs.getString("name", "");
            id = prefs.getString("id", "");
            email = prefs.getString("email", "");

        }
        Student student = new Student(id, name, email, 1, 1);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.titlebar);
        viewPager = findViewById(R.id.viewpager);
        // Create an adapter that knows which fragment should be shown on each page
        adapter = new HomeFragmentAdaper(this, getSupportFragmentManager(), student);
        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);
        // Give the TabLayout the ViewPager
        tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

}
