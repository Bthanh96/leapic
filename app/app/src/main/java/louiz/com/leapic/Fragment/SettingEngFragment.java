package louiz.com.leapic.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import louiz.com.leapic.R;
import louiz.com.leapic.activity.LoginActivity;
import louiz.com.leapic.adapter.TopicAdapter;
import louiz.com.leapic.model.Student;
import louiz.com.leapic.model.Topic;

import static android.content.Context.MODE_PRIVATE;

public class SettingEngFragment extends Fragment {
    private Student student;
    TextView txtName,txtDate,txtRank;
    CircleImageView imgProfile;
    Button btnLogout;
    GoogleApiClient mGoogleApiClient;
    public static final String MY_PREFS_NAME = "userData";
    public SettingEngFragment(){

    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View PageTree = inflater.inflate(R.layout.fragment_setting_eng,container,false);
        student = (Student) getArguments().getSerializable("student");
        return  PageTree;

    }

    @Override
    public void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnLogout = view.findViewById(R.id.btnLogout);
        txtName = view.findViewById(R.id.txtName);
        txtDate = view.findViewById(R.id.txtDate);
        txtRank = view.findViewById(R.id.txtRank);
        txtName.setText(student.getName());
        txtDate.setText(student.getDayScore()+"");
        txtRank.setText(student.getScore()+"");
        CircleImageView ggProfilePicture;
        ggProfilePicture = view.findViewById(R.id.ggProfilePicture);
        ProfilePictureView profilePictureView;
        profilePictureView = view.findViewById(R.id.friendProfilePicture);

        AccessToken token;
        token = AccessToken.getCurrentAccessToken();
        if(token!=null){
            profilePictureView.setVisibility(View.VISIBLE);
            ggProfilePicture.setVisibility(View.INVISIBLE);
            profilePictureView.setProfileId(student.getId());
        }
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected()){
            ggProfilePicture.setVisibility(View.VISIBLE);
            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String url = prefs.getString("urlPhoto", "");
            Glide
                    .with(getActivity())
                    .load(url)
                    .into(ggProfilePicture);
            profilePictureView.setVisibility(View.INVISIBLE);

        }

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AccessToken token;
                token = AccessToken.getCurrentAccessToken();
                if(token!=null){
                    LoginManager.getInstance().logOut();
                    Intent intent = new Intent(getActivity(),LoginActivity.class);
                    getActivity().startActivity(intent);
                }
                if(mGoogleApiClient != null && mGoogleApiClient.isConnected()){
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    Toast.makeText(getContext(),"Logged Out",Toast.LENGTH_SHORT).show();
                                    Intent i=new Intent(getContext(),LoginActivity.class);
                                    startActivity(i);
                                }
                            });
                }





            }
        });


    }






}
