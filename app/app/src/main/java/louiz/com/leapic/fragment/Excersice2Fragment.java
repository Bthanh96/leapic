package louiz.com.leapic.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.geniusforapp.fancydialog.FancyAlertDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;

import louiz.com.leapic.R;
import louiz.com.leapic.activity.ExcersiceActivity;
import louiz.com.leapic.activity.HomeEngActivity;
import louiz.com.leapic.activity.SplashActivity;
import louiz.com.leapic.customClass.MyBounceInterpolator;
import louiz.com.leapic.model.Word;

import static android.content.Context.MODE_PRIVATE;

public class Excersice2Fragment extends Fragment {


    String DATABASE_NAME="LeaPic.sqlite";
    String DB_PATH_SUFFIX = "/databases/";
    SQLiteDatabase database = null;
    private String categoryId;
    Button btnCheck;
    CardView card1,card2,card3,card4,card5,card6,card7,card8,card9,card10;
    TextView txt1,txt2,txt3,txt4,txt5,txtmean1,txtmean2,txtmean3,txtmean4,txtmean5,txtQues;
    TextView hint1,hint2,hint3,hint4,hint5,hint6,hint7,hint8,hint9,hint10;
    ArrayList<Word> wordsList1 = new ArrayList<>();
    ArrayList<Word> wordsList2 = new ArrayList<>();
    ArrayList<Word> ran_exWord1;
    ArrayList<Word> ran_exWord2;
    int check1 = -1,check2 = -1,count=0;
    CardView cardLeft,cardRight;
    private static int SPLASH_TIME_OUT1 = 1000;
    public Excersice2Fragment( ){

    };


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View PageTree = inflater.inflate(R.layout.fragment_excersice2_eng,container,false);

        return  PageTree;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ProcessCopyDatabase();

        txt1 = view.findViewById(R.id.txt1);
        txt2 = view.findViewById(R.id.txt2);
        txt3 = view.findViewById(R.id.txt3);
        txt4 = view.findViewById(R.id.txt4);
        txt5 = view.findViewById(R.id.txt5);
        txtmean1 = view.findViewById(R.id.txtmean1);
        txtmean2 = view.findViewById(R.id.txtmean2);
        txtmean3 = view.findViewById(R.id.txtmean3);
        txtmean4 = view.findViewById(R.id.txtmean4);
        txtmean5 = view.findViewById(R.id.txtmean5);
        card1 = view.findViewById(R.id.card1);
        card2 = view.findViewById(R.id.card2);
        card3 = view.findViewById(R.id.card3);
        card4 = view.findViewById(R.id.card4);
        card5 = view.findViewById(R.id.card5);
        card6 = view.findViewById(R.id.card6);
        card7 = view.findViewById(R.id.card7);
        card8 = view.findViewById(R.id.card8);
        card9 = view.findViewById(R.id.card9);
        card10 = view.findViewById(R.id.card10);
        hint1 = view.findViewById(R.id.hint1);
        hint2 = view.findViewById(R.id.hint2);
        hint3 = view.findViewById(R.id.hint3);
        hint4 = view.findViewById(R.id.hint4);
        hint5 = view.findViewById(R.id.hint5);
        hint6 = view.findViewById(R.id.hint6);
        hint7 = view.findViewById(R.id.hint7);
        hint8 = view.findViewById(R.id.hint8);
        hint9 = view.findViewById(R.id.hint9);
        hint10 = view.findViewById(R.id.hint10);
        txtQues = view.findViewById(R.id.txtQues);
        btnCheck = view.findViewById(R.id.btnCheck);
        showAllNames();
        addEvent();
    }



    private void showAllNames() {
        categoryId = getArguments().getString("categoryId");
        database = getContext().openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        Cursor cursor = database.rawQuery("select * FROM Word where TopicId = "+categoryId+" and Learned = 0",null);
        wordsList1.clear();
        while (cursor.moveToNext()){
            int idWord = cursor.getInt(0);
            String nameWord = cursor.getString(1);
            String spell = cursor.getString(3);
            String mean = cursor.getString(4);
            byte[] pic = cursor.getBlob(8);
            int learn = cursor.getInt(5);
            if(learn==0 && wordsList1.size()<5){
                Word word = new Word(idWord,nameWord,spell,mean,pic,learn);
                wordsList1.add(word);
                wordsList2.add(word);
            }


        }
        Random r = new Random();

        ran_exWord1 = new ArrayList<>();
        ran_exWord2 = new ArrayList<>();
        for(int i = 0;i<wordsList1.size();i+=0){
            int x = r.nextInt(wordsList1.size());
            ran_exWord1.add(wordsList1.get(x));
            wordsList1.remove(x);
        }

        for(int i = 0;i<wordsList2.size();i+=0){
            int x = r.nextInt(wordsList2.size());
            ran_exWord2.add(wordsList2.get(x));
            wordsList2.remove(x);
        }



        txt1.setText(ran_exWord1.get(0).getName());
        txt2.setText(ran_exWord1.get(1).getName());
        txt3.setText(ran_exWord1.get(2).getName());
        txt4.setText(ran_exWord1.get(3).getName());
        txt5.setText(ran_exWord1.get(4).getName());
        txtmean1.setText(ran_exWord2.get(0).getExMean());
        txtmean2.setText(ran_exWord2.get(1).getExMean());
        txtmean3.setText(ran_exWord2.get(2).getExMean());
        txtmean4.setText(ran_exWord2.get(3).getExMean());
        txtmean5.setText(ran_exWord2.get(4).getExMean());
        hint1.setText(ran_exWord1.get(0).getId()+"");
        hint2.setText(ran_exWord1.get(1).getId()+"");
        hint3.setText(ran_exWord1.get(2).getId()+"");
        hint4.setText(ran_exWord1.get(3).getId()+"");
        hint5.setText(ran_exWord1.get(4).getId()+"");
        hint6.setText(ran_exWord2.get(0).getId()+"");
        hint7.setText(ran_exWord2.get(1).getId()+"");
        hint8.setText(ran_exWord2.get(2).getId()+"");
        hint9.setText(ran_exWord2.get(3).getId()+"");
        hint10.setText(ran_exWord2.get(4).getId()+"");
        txtQues.setText("Nối Từ Tiếng Anh Đúng Với Nghĩa");
        cursor.close();
    }

    private void addEvent() {
        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cardClickLeft(card1,hint1);

            }
        });
        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickLeft(card2,hint2);
            }
        });
        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickLeft(card3,hint3);
            }
        });
        card4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickLeft(card4,hint4);
            }
        });
        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickLeft(card5,hint5);
            }
        });
        card6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickRight(card6,hint6);
            }
        });
        card7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickRight(card7,hint7);
            }
        });
        card8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickRight(card8,hint8);
            }
        });
        card9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickRight(card9,hint9);
            }
        });
        card10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClickRight(card10,hint10);
            }
        });
    }

    private void cardClickLeft(CardView card, TextView txt) {
        final Animation myAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 10);
        myAnim.setInterpolator(interpolator);
        card.startAnimation(myAnim);
        check1 = Integer.parseInt(txt.getText().toString());
        cardLeft = card;
        if(check2!=-1){
            if(check1==check2){
                resetCardLeft();
                resetCardRight();
                cardRight.setBackgroundColor(getResources().getColor(R.color.colorIcon));
                cardLeft.setBackgroundColor(getResources().getColor(R.color.colorIcon));
                disableClick();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        cardRight.setVisibility(View.INVISIBLE);
                        cardLeft.setVisibility(View.INVISIBLE);
                        check1=-1;
                        check2=-1;
                        count++;
                        if(count==5){
                            showEnd5Word();
                        }
                        resetClick();
                    }
                },SPLASH_TIME_OUT1);



            }else {
                resetCardLeft();
                resetCardRight();
                check1=-1;
                check2=-1;
            }
        }
        else {
            resetCardLeft();
            card.setCardElevation(0);
            card.setBackgroundColor(getResources().getColor(R.color.colorPicker));

        }


    }

    private void resetClick() {
        card1.setClickable(true);
        card2.setClickable(true);
        card3.setClickable(true);
        card4.setClickable(true);
        card5.setClickable(true);
        card6.setClickable(true);
        card7.setClickable(true);
        card8.setClickable(true);
        card9.setClickable(true);
        card10.setClickable(true);
    }

    private void disableClick() {
        card1.setClickable(false);
        card2.setClickable(false);
        card3.setClickable(false);
        card4.setClickable(false);
        card5.setClickable(false);
        card6.setClickable(false);
        card7.setClickable(false);
        card8.setClickable(false);
        card9.setClickable(false);
        card10.setClickable(false);

    }

    private void showEnd5Word() {
        FancyAlertDialog.Builder alert = new FancyAlertDialog.Builder(getContext())
                .setimageResource(R.drawable.done)
                .setTextTitle("Chúc Mừng")
                .setTextSubTitle("Bạn Đã Học Xong 5 Từ Vựng")
                .setBody("Bạn có muốn học tiếp ?")
                .setNegativeColor(R.color.colorNegative)
                .setNegativeButtonText("Để Sau")
                .setOnNegativeClicked(new FancyAlertDialog.OnNegativeClicked() {
                    @Override
                    public void OnClick(View view, Dialog dialog) {
                        Intent intent = new Intent(getContext(), HomeEngActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    }
                })
                .setPositiveButtonText("Tiếp Tục")
                .setPositiveColor(R.color.colorPositive)
                .setOnPositiveClicked(new FancyAlertDialog.OnPositiveClicked() {
                    @Override
                    public void OnClick(View view, Dialog dialog) {
                        dialog.dismiss();
                    }
                })
                .setBodyGravity(FancyAlertDialog.TextGravity.CENTER)
                .setTitleGravity(FancyAlertDialog.TextGravity.CENTER)
                .setSubtitleGravity(FancyAlertDialog.TextGravity.CENTER)
                .setButtonsGravity(FancyAlertDialog.PanelGravity.CENTER)
                .setCancelable(false)
                .build();
        alert.show();
    }

    private void cardClickRight(CardView card, TextView txt) {
        final Animation myAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 10);
        myAnim.setInterpolator(interpolator);
        card.startAnimation(myAnim);
        check2 = Integer.parseInt(txt.getText().toString());
        cardRight = card;
        if(check1!=-1){
            if(check1==check2){
                resetCardRight();
                resetCardLeft();
                cardRight.setBackgroundColor(getResources().getColor(R.color.colorIcon));
                cardLeft.setBackgroundColor(getResources().getColor(R.color.colorIcon));
                disableClick();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        cardRight.setVisibility(View.INVISIBLE);
                        cardLeft.setVisibility(View.INVISIBLE);
                        check1=-1;
                        check2=-1;
                        count++;
                        if(count==5){
                            showEnd5Word();
                        }
                        resetClick();
                    }
                },SPLASH_TIME_OUT1);
            }else {
                resetCardLeft();
                resetCardRight();
                check1=-1;
                check2=-1;
            }
        }
        else {
            resetCardRight();
            card.setCardElevation(0);
            card.setBackgroundColor(getResources().getColor(R.color.colorPicker));

        }

    }

    private void resetCardLeft() {
        card1.setCardElevation(10);
        card2.setCardElevation(10);
        card3.setCardElevation(10);
        card4.setCardElevation(10);
        card5.setCardElevation(10);
        card1.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card2.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card3.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card4.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card5.setBackgroundColor(getResources().getColor(R.color.colorWhite));
    }
    private void resetCardRight() {
        card6.setCardElevation(10);
        card7.setCardElevation(10);
        card8.setCardElevation(10);
        card9.setCardElevation(10);
        card10.setCardElevation(10);
        card6.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card7.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card8.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card9.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        card10.setBackgroundColor(getResources().getColor(R.color.colorWhite));
    }

    private  void ProcessCopyDatabase(){
        File dbFile = getActivity().getDatabasePath(DATABASE_NAME);
        if(!dbFile.exists()){
            try {
                CopyDataBaseFromAsset();

            }catch (Exception ex){
                Toast.makeText(getContext(), ex.toString(),Toast.LENGTH_LONG).show();

            }
        }
    }

    private void CopyDataBaseFromAsset() {
        try{
            InputStream myInput = getContext().getAssets().open(DATABASE_NAME);
            String outFileName = GetPath();
            File f = new File(getContext().getApplicationInfo().dataDir+DB_PATH_SUFFIX);
            if(!f.exists()){
                f.mkdir();
            }
            OutputStream myOutput = new FileOutputStream(outFileName);

            byte[] buffer = new byte[1024];
            int lengt;
            while((lengt = myInput.read(buffer))>0){
                myOutput.write(buffer,0,lengt);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();

        }catch (Exception ex){
            Log.e("error_whilecopy",ex.toString());
        }
    }

    private String GetPath(){
        return getContext().getApplicationInfo().dataDir+DB_PATH_SUFFIX+DATABASE_NAME;

    }

}
