package louiz.com.leapic.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;


import louiz.com.leapic.R;

public class LoginActivity extends AppCompatActivity {


    LoginButton button;

    CallbackManager callbackManager;
    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build();
    public static final String MY_PREFS_NAME = "userData";
    GoogleSignInClient mGoogleSignInClient;
    SignInButton signInButton;
    private static final int RC_SIGN_IN = 1;
    AccessToken token;
    GoogleSignInAccount account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token = AccessToken.getCurrentAccessToken();
        account = GoogleSignIn.getLastSignedInAccount(this);
        if (token == null && account == null) {
            setContentView(R.layout.activity_login);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().hide();

            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            signInButton = findViewById(R.id.sign_in_button);
            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }
            });

            FacebookSdk.sdkInitialize(getApplicationContext());
            button = findViewById(R.id.login_button);
            button.setReadPermissions("email","public_profile");
            callbackManager = CallbackManager.Factory.create();

            button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    String userId = loginResult.getAccessToken().getUserId();
                    GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            displayUser(object);
                        }
                    });
                    Bundle parameter = new Bundle();
                    parameter.putString("fields","first_name,last_name,email,id");
                    graphRequest.setParameters(parameter);
                    graphRequest.executeAsync();
                }
                @Override
                public void onCancel() {
                }
                @Override
                public void onError(FacebookException error) {
                }
            });



        }else {
            Intent intent = new Intent(LoginActivity.this,HomeEngActivity.class);
            startActivity(intent);
        }
    }


    private void displayUser(JSONObject object) {
        String firstname,lastname,email,id;
        firstname = "";
        lastname = "";
        email = "";
        id = "";

        try {
            firstname = object.getString("first_name");
            lastname = object.getString("last_name");
            email = object.getString("email");
            id = object.getString("id");
            Intent intent = new Intent(LoginActivity.this,HomeEngActivity.class);
            intent.putExtra("name",firstname+" "+lastname);
            intent.putExtra("email",email);
            intent.putExtra("id",id);
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("id", id);
            editor.putString("name",firstname+" "+lastname );
            editor.putString("email",email);

            editor.commit();
            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            String name = account.getDisplayName();
            String email = account.getEmail();
            String id = account.getId();
            Intent intent = new Intent(LoginActivity.this,HomeEngActivity.class);
            intent.putExtra("name",name);
            intent.putExtra("email",email);
            intent.putExtra("id",id);
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("id", id);
            editor.putString("urlPhoto",account.getPhotoUrl().toString());
            editor.putString("name",name );
            editor.putString("email",email);
            editor.commit();
            startActivity(intent);
        } catch (ApiException e) {
            Toast.makeText(this,e.getStackTrace().toString(),Toast.LENGTH_LONG).show();
        }
    }



}
