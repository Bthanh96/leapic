package louiz.com.leapic.model;

import java.io.Serializable;

public class Student implements Serializable {

    private String id,address,name,email,phone;
    private int score,dayScore;

    public Student(String id, String address, String name, String email, String phone, int score, int dayScore) {
        this.id = id;
        this.address = address;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.score = score;
        this.dayScore = dayScore;
    }
    public Student(String id, String name, String email, int score, int dayScore) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.score = score;
        this.dayScore = dayScore;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDayScore() {
        return dayScore;
    }

    public void setDayScore(int dayScore) {
        this.dayScore = dayScore;
    }
}
