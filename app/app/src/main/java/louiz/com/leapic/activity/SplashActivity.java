package louiz.com.leapic.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


import com.github.glomadrian.grav.GravView;

import louiz.com.leapic.R;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT1 = 3000;
    private static int SPLASH_TIME_OUT2 = 2500;
    GravView gravView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().hide();
        gravView =findViewById(R.id.grav);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        },SPLASH_TIME_OUT1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gravView.animate().alpha(0.0f);
            }
        },SPLASH_TIME_OUT2);

    }
}
